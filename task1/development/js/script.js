var numberElem = document.getElementById('numberInput');
    
(function(){
    var resultsElem = document.getElementById('resultsDiv');
    
    // method that links the generator to the interface
    function printSeries(e) {
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
        
        var number = numberElem.value,
            series = generateSeries_optimized( number );
        
        resultsElem.innerHTML = series;
    }
    window.printSeries = printSeries;
})();


var calculateForm = document.getElementById('calculateForm');

if (window.addEventListener) {
    calculateForm.addEventListener('submit', printSeries, false);
    // Old-style placeholder support
    numberElem.addEventListener('focus', function(){ if (this.value == 'Enter a number') this.value = ''; }, false);
    numberElem.addEventListener('blur', function(){ if (this.value == '') this.value = 'Enter a number'; }, false);
// IE Version
} else if (window.attachEvent) {
    calculateForm.attachEvent('onsubmit', printSeries);
    // Old style placeholder support, IE version
    numberElem.attachEvent('onfocus', function(){ if (numberElem.value == 'Enter a number') numberElem.value = ''; });
    numberElem.attachEvent('onblur', function(){ if (numberElem.value == '') numberElem.value = 'Enter a number'; });
}