/**
 * Optimized version of the generateSeries function which generates and returns a number series according to the algorithm.
 * @param {integer} number Generate series up to this number
 */
function generateSeries_optimized (number) {
    var series = '';

    if (number == undefined || !/^\d+$/.test(number) || number === 0) 
        return 'Please enter a positive integer!';
    
    for (var i = 1; i <= number; i++) {
        var d2 = !(i % 2),
            d3 = !(i % 3);
        
        if (d2 || d3)
            if (d2)
                if (d3)
                    series += '<div class="foo bar">foo bar</div>';
                else
                    series += '<div class="foo">foo</div>';
            else
                series += '<div class="bar">bar</div>';
        else 
            series += '<div class="number">' + i + '</div>';
    }

    return series;
}

/**
 * Returns a string with the generated series according to the algorithm, up to the number in the argument.
 * @param {integer} number Generate series up to this number
 * @param {string} element The element to wrap each row with, as a string (e.g. generateSeries(15, 'span');). Default 'div'.
 */
function generateSeries (number, element) {
    var series = '',
        el     = 'div';

    if (number == undefined || !/^\d+$/.test(number) || number === 0) 
        return 'Please enter a positive integer!';
    
    if (element != undefined)
        el = element;
    
    for (var i = 1; i <= number; i++) {
        var d2 = !(i % 2),
            d3 = !(i % 3),
            content = (d2 ? 'foo' + (d3 ? ' ' : '') : '') + (d3 ? 'bar' : '');
           
        series += '<' + el + ' class="' +
                    (d2 || d3 ? 
                        content + '">' + content : 
                        'number">' + i)
                  + '</' + el + '>'
                  + (i != number ? '\n' : '');
    }

    return series;
}