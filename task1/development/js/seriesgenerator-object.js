(function(){
    /**
     * Series Generator constructor. Sets the maximum series number from its first argument.
     * @param {integer} number The maximum number of the generated series
     * @constructor
     */
    function SeriesGenerator( number ) {
        this._error = null;
        this._max    = 0;


        if (number != undefined)
            this.maxNumber(number);
    }
    
    /**
     * Returns a string with the generated series up to the number in the argument or the one
     * already set in the class (the number in the argument has priority)
     * @param {integer} number Generate series up to this number (optional)
     * @property {integer} _max This method optionally sets the _max property
     */
    SeriesGenerator.prototype.generate = function (number) {
        var series = '';
        
        if (number != undefined && !this.maxNumber(number) || this._max == 0)
            return this.error('Please enter a positive integer!'); // write the error message explicitly for the this._max case
 
        for (var i = 1; i <= this._max; i++)
            series += printNumber(i) + (i != this._max ? '\n' : '');
 
        return series;
    }

     /**
     * Returns the maximum number if no parameter is set
     * or set the maximum number if the first argument is an integer.
     * @param {integer} value The new value of the maximum number
     * @property {integer} _max This method returns or sets the _max property
     */
    SeriesGenerator.prototype.maxNumber = function ( value ) {
        if ( !arguments.length )
            return this._max;

        if ( !/^\d+$/.test(value) || value === 0 ) {
            this.error('Please enter a positive integer!');
            return false;            
        }

        this._max = parseInt(value);
        return true;
    }

    /**
     * Returns the last error (if any) or sets a new one
     * @param {string} error The error to be set
     * @property {string} _error This method returns or sets the _error property
     */
    SeriesGenerator.prototype.error = function( error ) {
        if ( !arguments.length )
            return this._error;

        this._error = error;
        return error;    
    }

    /**
     * Function that applies the following algorithm to a single number:<br />
     * - can the number be divided by 2? Return 'foo'.<br />
     * - can the number be divided by 3? Return 'bar'.<br />
     * - can the number be divided by both 2 and 3? Return 'foo bar' instead.<br />
     * - otherwise, return the number<br />
     * @param {integer} number
     */
    function printNumber (number) {
        var d2 = !(number % 2),
            d3 = !(number % 3),
            string = d2 || d3 ? 
                        (d2 ? 'foo' + (d3 ? ' ' : '') : '') + (d3 ? 'bar' : '') : 
                        number;

        return string;
    }
    
    window.SeriesGenerator = SeriesGenerator;
    
    // This is left here for demo purposes, so that Test 2 can work. It won't be present in production code
    window.printNumber = printNumber;
})();