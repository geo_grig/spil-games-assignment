module('Object approach');
test( 'Getters and setters: filtering and validation', function(){    
    var gen = new SeriesGenerator( 3 );
    equal( gen.maxNumber(), 3, 'Is the number set correctly using the constructor?');
    
    equal( gen.maxNumber( 9 ), true, 'Is the number set correctly using the setter? 1/2' );
    equal( gen.maxNumber(), 9, 'Is the number set correctly using the setter? 2/2');
    
    equal( gen.maxNumber( '7' ), true, 'Numbers in strings' );
    equal( gen.maxNumber( 'a' ), false, 'Strings' );
    
    equal( gen.maxNumber( [ 'test' ] ), false, 'Arrays' );
    equal( gen.maxNumber( { 'test' : 'test' } ), false, 'Objects' );
    
    equal( gen.maxNumber( [] ), false, 'Empty array' );
    equal( gen.maxNumber( {} ), false, 'Empty object' );
    
    equal( gen.maxNumber( 5.25 ), false, 'Floats' );
    equal( gen.maxNumber( '5.25' ), false, 'Floats in strings' );
    equal( gen.maxNumber( -110 ), false, 'Negative numbers' );
    
    equal( gen.maxNumber(), 7, 'Final invalid argument test. The last valid number should be a 7' );
});


test('Algorithm implementation test', function(){
    equal( printNumber(1), '1', 'printNumber(1) = 1' );
    equal( printNumber(2), 'foo', "printNumber(2) = 'foo'" );
    equal( printNumber(3), 'bar', "printNumber(3) = 'bar'" );
    equal( printNumber(4), 'foo', "printNumber(4) = 'foo'" );
    equal( printNumber(5), '5', "printNumber(5) = '5'" );
    equal( printNumber(6), 'foo bar', "printNumber(6) = 'foo bar'" );
    equal( printNumber(351), 'bar', "printNumber(351) = 'bar'" );
    equal( printNumber(4524), 'foo bar', "printNumber(4524) = 'foo bar'" );
    equal( printNumber(645230), 'foo', "printNumber(645230) = 'foo'" );
    equal( printNumber(438952878), 'foo bar', "printNumber(438952878) = 'foo bar'" );
});


test('SeriesGenerator object string generation test', function(){
    var gen = new SeriesGenerator( 7 );
    equal( gen.generate(), '1\nfoo\nbar\nfoo\n5\nfoo bar\n7', "gen.generate(7) = '1\nfoo\nbar\nfoo\n5\nfoo bar\n7\n'" );
    equal( gen.generate('invalid'), 'Please enter a positive integer!', "gen.generate('invalid') = 'Please enter a positive integer!'" );
    equal( gen.generate(0), 'Please enter a positive integer!', "gen.generate(0) = 'Please enter a positive integer!'" );
    equal( gen.generate(1), '1', "gen.generate(1) = '1'" );
    equal( gen.generate(2), '1\nfoo', "gen.generate(2) = '1\nfoo'" );
    equal( gen.generate(3), '1\nfoo\nbar', "gen.generate(3) = '1\nfoo\nbar'" );
    
    
    gen = new SeriesGenerator();
    equal( gen.generate(), 'Please enter a positive integer!', "Test with undefined data: var gen = new SeriesGenerator(); gen.generate() == 'Please enter a positive integer!' ?" );
});


module('Function approach');
test( 'Getters and setters: filtering and validation', function(){
    equal( generateSeries(), 'Please enter a positive integer!', 'Test with undefined data: generateSeries();');
    
    equal( generateSeries('a'), 'Please enter a positive integer!', 'Strings' );
    
    equal( generateSeries([ 'test' ]), 'Please enter a positive integer!', 'Arrays' );
    equal( generateSeries({ 'test' : 'test' }), 'Please enter a positive integer!', 'Objects' );
    
    equal( generateSeries( [] ), 'Please enter a positive integer!', 'Empty array' );
    equal( generateSeries( {} ), 'Please enter a positive integer!', 'Empty object' );
    
    equal( generateSeries( 5.25 ), 'Please enter a positive integer!', 'Floats' );
    equal( generateSeries( '5.25' ), 'Please enter a positive integer!', 'Floats in strings' );
    equal( generateSeries( -110 ), 'Please enter a positive integer!', 'Negative numbers' );
});


test('generateSeries() algorithm implementation and string generation test', function(){
    equal( generateSeries(0), 'Please enter a positive integer!', "generateSeries(0) = 'Please enter a positive integer!'" );
    equal( generateSeries(1), '<div class="number">1</div>', "generateSeries(1) = '<div class=\"number\">1</div>'" );
    equal( generateSeries(2), '<div class=\"number\">1</div>\n<div class=\"foo\">foo</div>', "generateSeries(2) = '<div class=\"number\">1</div>\\n<div class=\"foo\">foo</div>'" );
    equal( generateSeries(3), '<div class=\"number\">1</div>\n<div class=\"foo\">foo</div>\n<div class=\"bar\">bar</div>', "generateSeries(3) = '<div class=\"number\">1</div>\\n<div class=\"foo\">foo</div>\\n<div class=\"bar\">bar</div>'" );
    
});

module('Performance test');
test('SeriesGenerator.generate(1700000)', function(){
    expect(0);
    var gen = new SeriesGenerator();
    gen.generate(1700000);
});

test('generateSeries(1700000)', function(){
    expect(0);
    generateSeries(1700000);
});

test('generateSeries_optimized(1700000)', function(){
    expect(0);
    generateSeries_optimized(1700000);
});