(function(){
    // Dutch income tax brackets
    var brackets = [
        {maxIncome: 18218, maxAge: 64, tax: 33.45},
        {maxIncome: 18218, minAge: 64, tax: 15.55},
        {minIncome: 18218, maxIncome: 32738, maxAge: 64, tax: 41.95},
        {minIncome: 18218, maxIncome: 32738, minAge: 64, tax: 24.05},
        {minIncome: 32738, maxIncome: 54367, tax: 42},
        {minIncome: 54367, tax: 52}
    ];
    
    // Different period versions accepted as parameters
    var monthly = [ 'm', 'month', 'monthly' ],
        yearly  = [ 'y', 'year', 'yearly' ];
    
    
    /**
     * Income Tax Generator constructor. Can set the income, age and period if the corresponding arguments (in that order) are present.
     * @param {integer} income A person's income
     * @param {integer} age Person's age
     * @param {string} period Income period. Can be 'monthly'/'month'/'m' or 'yearly'/'year'/'y' (optional - default 'yearly')
     * @constructor
     */
    function IncomeTax( income, age, period ) {
        this._error = null;
        this._income = 0;
        this._age    = 1;
        
        if (income != undefined) 
            this.income( period, income );
        
        if (age != undefined)
            this.age( age );
    }
    
    /**
     * Calculates and returns the income tax.
     */
    IncomeTax.prototype.get = function () {
        // Return the error if there's an error
        if (this.error()) {
            var err = this.error();
            // clear the error first
            this.error('');
            return err;
        }
			
        return calculateTax( this._income, this._age );
    }
    
    /**
     * Calculates and returns the income tax percentage.
     */
    IncomeTax.prototype.getPercentage = function () {
        // Return the error if there's an error
        if (this.error())
            return this.error();
			
        return calculateTaxPercentage( this._income, this._age );
    }

    /**
     * Returns or sets the income. Can also specify income period as 'monthly'/'month'/'m' or 'yearly'/'year'/'y'.
     * @param {string} period Income period. Can be 'monthly'/'month'/'m' or 'yearly'/'year'/'y' (optional - default 'yearly')
     * @param {integer} value The new value of the income
     * @property {integer} _income This method returns or sets the _income property
     */
    IncomeTax.prototype.income = function ( period, value ) {
        if (!arguments.length)
            return this._income;
        else if (arguments.length == 1)
            if (monthly.indexOf(period) != -1)
                return parseInt( this._income / 12 );
            else
                return this._income;
        
        if (!/^\d+$/.test(value)) {
            this.error('Please enter a valid income!');
            return false;            
        
        }
        
        value = parseInt(value);
        
        if (monthly.indexOf(period) != -1)
            value *= 12;

        this._income = value;
        return true;
    }
    
    /**
     * Returns or sets the age.
     * @param {integer} value The new age
     * @property {integer} _income This method returns or sets the _age property
     */
    IncomeTax.prototype.age = function ( value ) {
        if (!arguments.length)
            return this._age;
        
        if (!/^\d+$/.test(value)) { 
            this.error('Please enter a valid age!');
            return false;            
        } 
        this._age = parseInt(value);
        return true;
    }
    
    /**
     * Returns the last error (if any) or sets a new one
     * @param {string} error The error to be set
     * @property {string} _error This method returns or sets the _error property
     */
    IncomeTax.prototype.error = function( error ) {
        if ( !arguments.length )
            return this._error;

        this._error = error;
        return error;    
    }
    
    /**
     * Calculate the income tax for a given income and age
     * @param {integer} income The income to calculate tax for
     * @param {integer} age The age to calculate tax for
     */
    function calculateTax( income, age ) {
        var percent = calculateTaxPercentage( income, age );
        
        return (income * percent / 100).toFixed(2);
    }
    
    
    /**
     * Calculate the income tax percentage for a given income and age (created mostly for testing purposes)
     * @param {integer} income The income to calculate tax percentage for
     * @param {integer} age The age to calculate tax percentage for
     */
    function calculateTaxPercentage ( income, age ) {
        var br;
        for (var i in brackets) {
            br = brackets[i];
            
            if (
                    br.minIncome && br.minIncome > income  ||
                    br.maxIncome && br.maxIncome <= income ||
                    br.minAge && br.minAge > age           ||
                    br.maxAge && br.maxAge <= age
                )
                continue;
            
            break;
        }
        
        return br.tax;
    }
    
    /**
     * This implements Array.indexOf, in case the browser doesn't support it (for IE < 8 compatibility)
     */
    if(!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(needle) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === needle) {
                    return i;
                }
            }
            return -1;
        };
    }
    
    window.IncomeTax = IncomeTax;
    
    
    // Aded this lines here so tests would work. They aren't in production code
    window.calculateTax = calculateTax;
    window.calculateTaxPercentage = calculateTaxPercentage;
})();