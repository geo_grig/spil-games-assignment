// Placeholder support for older browsers 
(function(){
    var placeholderSupport = (function(){
        var i = document.createElement('input');
        return 'placeholder' in i;
    })();

    if (!placeholderSupport) {
        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                  input.val('');
                  input.removeClass('placeholder');
            }
          }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                  input.addClass('placeholder');
                  input.val(input.attr('placeholder'));
            }
          }).blur();
    }
})();

(function(){
    // One time only initialization here, for higher execution speed
    var tax = new IncomeTax();

    $('.incomeCalculator form').on('submit', function(e){
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
        
        var $parentElem = $(this).parents('.incomeCalculator'), // get the current form instance
            $resultElem = $('#resultsDiv'),
            age     = $('.ageInput', $parentElem).val(),
            income  = $('.incomeInput', $parentElem).val(),
            period  = $('select', $parentElem).val(),
            ret     = '';
            
        if (!tax.age( age ) || !tax.income( period, income ))
            ret = tax.get();
        else
            ret = tax.get() + ' (' + tax.getPercentage() + '%) / year';
            
        $resultElem.html( ret );        

    });
})();