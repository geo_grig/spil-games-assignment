test( 'Input parameters filtering and validation', function(){    
    var tax = new IncomeTax( 60000, 23 );
    equal( tax.income(), 60000, 'Constructor income setter: var tax = new IncomeTax( 60000, 23 ); tax.income() == 60000 ?');
    equal( tax.age(), 23, 'Constructor age setter: tax.age() == 23 ?');
    
    equal( tax.income('m'), 5000, 'Income getter: tax.income(\'m\') == 5000 ?');
    tax.income('m', 7000);
    equal( tax.income(), 84000, 'Income getter/setter: tax.income(\'m\', 7000); tax.income() == 84000 ?');
    equal( tax.income('y'), 84000, 'Income getter: tax.income(\'y\') == 84000 ?');
    equal( tax.income('y', 50000), true, 'Income setter: tax.income(\'y\', 50000) == true ?');
    equal( tax.income('m'), 4166, 'Income getter: tax.income(\'m\') == 4166 ?');
    
    equal ( tax.income('m', 'aa'), false, 'Invalid income' );
    equal ( tax.income('y', [] ), false, 'Invalid income' );
    equal ( tax.age( 'invalid' ), false, 'Invalid age' );
    
    tax.age(54);
    equal( tax.age(), 54, 'Age setter/getter: tax.age(54); tax.age() == 54 ?');
});


test('Income tax percentage calculator', function(){
    equal ( calculateTaxPercentage( 0, 18 ), 33.45, 'Age: 18, Income: 0 => 33.45% taxes. calculateTaxPercentage( 0, 18 ) == 33.45' );
    equal ( calculateTaxPercentage( 10000, 30 ), 33.45, 'Age: 30, Income: 10000 => 33.45% taxes. calculateTaxPercentage( 10000, 30 ) == 33.45' );
    equal ( calculateTaxPercentage( 16000, 70 ), 15.55, 'Age: 70 Income: 16000 => 15.55% taxes. calculateTaxPercentage( 16000, 70 ) == 15.55' );
    equal ( calculateTaxPercentage( 18000, 43 ), 33.45, 'Age: 43, Income: 18000 => 33.45% taxes. calculateTaxPercentage( 18000, 43 ) == 33.45' );
    equal ( calculateTaxPercentage( 18218, 64 ), 24.05, 'Age: 64, Income: 18218 => 24.05% taxes. calculateTaxPercentage( 18218, 64 ) == 24.05' );
    equal ( calculateTaxPercentage( 33000, 28 ), 42, 'Age: 28, Income: 33000 => 42% taxes. calculateTaxPercentage( 33000, 28 ) == 42' );
    equal ( calculateTaxPercentage( 60000, 23 ), 52, 'Age: 23, Income: 60000 => 52% taxes. calculateTaxPercentage( 60000, 23 ) == 52' );
});


test('Income tax calculator', function(){
    equal ( calculateTax( 0, 18 ), 0, 'Age: 18, Income: 0 => 0 (33.45%) taxes. calculateTax( 0, 18 ) == 0' );
    equal ( calculateTax( 10000, 30 ), 3345, 'Age: 30, Income: 10000 => 3345 taxes (33.45%). calculateTax( 10000, 30 ) == 3345' );
    equal ( calculateTax( 16000, 70 ), 2488, 'Age: 70 Income: 16000 => 2488 taxes (15.55%). calculateTax( 16000, 70 ) == 2488' );
    equal ( calculateTax( 18000, 43 ), 6021, 'Age: 43, Income: 18000 => 6021 taxes (33.45%). calculateTax( 18000, 43 ) == 6021' );
    equal ( calculateTax( 18218, 64 ), 4381.43, 'Age: 64, Income: 18218 => 4381.43 taxes (24.05%). calculateTax( 18218, 64 ) == 4381.43' );
    equal ( calculateTax( 33000, 28 ), 13860, 'Age: 28, Income: 33000 => 13860 taxes (42%). calculateTax( 33000, 28 ) == 13860' );
    equal ( calculateTax( 60000, 23 ), 31200, 'Age: 23, Income: 60000 => 31200 taxes (52%). calculateTax( 60000, 23 ) == 31200' );
});


test('Income tax calculator interface', function(){
    equal ( new IncomeTax( 0, 18 ).get(), 0, 'Age: 18, Income: 0 => 0 (33.45%) taxes. calculateTax( 0, 18 ) == 0' );
    equal ( new IncomeTax( 10000, 30 ).get(), 3345, 'Age: 30, Income: 10000 => 3345 taxes (33.45%). calculateTax( 10000, 30 ) == 3345' );
    equal ( new IncomeTax( 16000, 70 ).get(), 2488, 'Age: 70 Income: 16000 => 2488 taxes (15.55%). calculateTax( 16000, 70 ) == 2488' );
    
    var inc = new IncomeTax();
    inc.income( 'm', 1500 );
    equal ( inc.get(), 6021, 'Age: 43, Income: 18000 => 6021 taxes (33.45%). calculateTax( 18000, 43 ) == 6021' );
    
    inc.income( 'm', 'aa' );
    equal ( inc.get(), 'Please enter a valid income!', 'Invalid income' );
    
    equal ( new IncomeTax( 18218, 64 ).get(), 4381.43, 'Age: 64, Income: 18218 => 4381.43 taxes (24.05%). calculateTax( 18218, 64 ) == 4381.43' );
    equal ( new IncomeTax( 33000, 28 ).get(), 13860, 'Age: 28, Income: 33000 => 13860 taxes (42%). calculateTax( 33000, 28 ) == 13860' );
    equal ( new IncomeTax( 60000, 23 ).get(), 31200, 'Age: 23, Income: 60000 => 31200 taxes (52%). calculateTax( 60000, 23 ) == 31200' );
    
    
});