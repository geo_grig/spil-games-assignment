var playersEl = document.querySelectorAll('.videoPlayer'),
    players   = [];

// Initialize and save the references of all the corresponding elements in the page
for (var i in playersEl) {
    if (playersEl[i] instanceof HTMLElement)
        players.push(new VideoPlayer(playersEl[i]));
}