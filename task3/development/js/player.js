/**
 * Metod that generates the video player inside an HTML element. It can be stored for later use.
 * @param {HTMLElement|String) element The element where to generate the video player
 * @constructor
 */
function VideoPlayer( element ) {
    this.paused = true;
    this.fullscreen = false;
    
    this._width = 640;
    this._height = 360;
    
    if (typeof element == 'string') {
        element = document.getElementById( element );
    } else if (!(element instanceof HTMLElement))
        throw new Error('You must pass the element id as a string or the dom element itself!');

    this.rootEl         = element;
    this.init();
}

/**
 * Video player initialization function. Initializes internal variables and controls
 */
VideoPlayer.prototype.init = function () {
    this.videoEl        = this.rootEl.querySelectorAll('video')[0];
    this.controlsEl     = this.rootEl.querySelectorAll('.controls')[0]; 
    
    // Play/pause controls
    this.playEl         = this.controlsEl.querySelectorAll('.play')[0];
    var playPauseCtls   = getChildren(this.playEl);
    this.playButtonEl   = playPauseCtls[0];
    this.pauseButtonEl  = playPauseCtls[1];
    
    // Seek controls
    this.seekEl         = this.controlsEl.querySelectorAll('.seek')[0];
    this.seekBarEl      = getFirstChild(this.seekEl);
    this.seekProgressEl = getFirstChild(this.seekBarEl);
    
    // Time controls
    this.timeEl         = this.controlsEl.querySelectorAll('.time')[0];
    var timeDisplays    = getChildren(this.timeEl);
    this.curTimeEl      = timeDisplays[0];
    this.totalTimeEl    = timeDisplays[1];
    
    // Volume controls
    this.volumeEl       = this.controlsEl.querySelectorAll('.volume')[0];
    this.volumeButtonEl = getFirstChild(this.volumeEl);
    
    // Full screen controls
    this.fullScreenEl   = this.controlsEl.querySelectorAll('.fullscreen')[0];
    this.fullScreenButtonEl = getFirstChild(this.fullScreenEl);
    
    addEventListener( document, 'ready', (function(){
        // initialize progress bar
        this.initProgressBar();
        // initialize time controls
        this.initTime();
        // initialize video events
        this.initEvents();
    }).call(this));
    
}

/**
 * Initializes the progress and control bars
 */
VideoPlayer.prototype.initProgressBar = function () { 
    this.controlsEl.style.width = (this.videoEl.offsetWidth - 25) + "px"; 
    this.seekEl.style.width = (parseInt(this.controlsEl.style.width) - 200) + "px";
    this.seekBarEl.style.width = (parseInt(this.seekEl.width) - 15) + "px";
}

/**
 * Initializes time controls with video details
 */
VideoPlayer.prototype.initTime = function() {
    var video = this;
    
    // initialize time controls only after video metadata has been loaded
    addEventListener( this.videoEl, 'loadedmetadata', function(){
        video.updateTotalTime();
    });
}

/**
 * Initializes video events (play, pause, etc)
 */
VideoPlayer.prototype.initEvents = function() {
    var video = this;
    
    // Play button click event
    addEventListener( this.playButtonEl, 'click', function(){video.play();} );
    
    // Pause button click event
    addEventListener( this.pauseButtonEl, 'click', function(){video.pause();} );
    
    // Progress bar click event
    addEventListener( this.seekBarEl, 'mousedown', function(e){video._setSeekPosition(e);} );
    
    // Play/pause for clicking the video itself
    addEventListener( this.videoEl, 'click', function(){
        if (video.paused)
            video.play();
        else 
            video.pause();
    });
    
    // Volume control click event
    addEventListener( this.volumeButtonEl, 'click', function(e){
        var vol = (e.pageX - findPosX(video.volumeButtonEl)) / video.volumeButtonEl.offsetWidth;
        video.setVolume(vol);
    });
    
    // Fullscreen event
    addEventListener( this.fullScreenButtonEl, 'click', function(){video.setFullScreen();});
    
    // Video ended event
    addEventListener( this.videoEl, 'ended', function(e){
        video.pause();
        video.rootEl.parentNode.innerHTML += '<p>Video has finished playing!</p>';
    });
}

/**
 * Starts the video 
 */
VideoPlayer.prototype.play = function(){
    this.paused = false;
    this.videoEl.play();
    this.startTrackProgress();
    
    this.playButtonEl.style.display = 'none';
    this.pauseButtonEl.style.display = 'block';
}

/**
 * Pauses the video
 */
VideoPlayer.prototype.pause = function() {
    this.paused = true;
    this.videoEl.pause();
    this.stopTrackProgress();
    
    this.pauseButtonEl.style.display = 'none';
    this.playButtonEl.style.display = 'block';
}

/**
 * Start tracking video progress
 */
VideoPlayer.prototype.startTrackProgress = function() {
  var player = this;
  
  this.progressInterval = setInterval(function(){player.updateSeekBar();}, 25);
}
 
/**
* Stops tracking video progress
*/
VideoPlayer.prototype.stopTrackProgress = function() {
  clearInterval(this.progressInterval);
}

/**
 * Updates the seek bar and the current time control with the current video progress
 */
VideoPlayer.prototype.updateSeekBar = function() {
    // update seek property
    this.progress = this.videoEl.currentTime / this.videoEl.duration;
    
    // update seek bar
    this.seekProgressEl.style.width = (this.progress * this.seekBarEl.offsetWidth) + "px";
    
    // update current displayed time
    this.updateCurrentTime();
}

/**
 * Updates the total time control with the total video time
 */
VideoPlayer.prototype.updateTotalTime = function() {
    this.totalTimeEl.innerHTML = formatTime(this.videoEl.duration);
}

/**
 * Updates the time in the control with the current video time
 */
VideoPlayer.prototype.updateCurrentTime = function() {
    this.curTimeEl.innerHTML = formatTime(this.videoEl.currentTime);
}

/**
 * Internal event handler that sets the seek bar position
 */
VideoPlayer.prototype._setSeekPosition = function(e) { 
    // Save 'this' instance
    var video = this; 
    
    // Stop time tracking
    this.stopTrackProgress();
    
    // Disable text selection
    document.body.focus(); document.onselectstart = function () { return false; };

    // Pause the video if it's running
    if (!this.videoEl.paused) {
        var resumeVideo = true;
        this.videoEl.pause();
    }
    
    var percent = Math.max(0, Math.min(1, (e.pageX - findPosX(this.seekBarEl)) / this.seekBarEl.offsetWidth));
    
    document.onmousemove = function(e) {
        // update percentage, video progress and the progress bar if the user moves the mouse
        percent = Math.max(0, Math.min(1, (e.pageX - findPosX(video.seekBarEl)) / video.seekBarEl.offsetWidth));
        video.setVideoProgress(percent);
    }
    
    document.onmouseup = function() {
        // restore text selection
        document.onselectstart = function () { return true; };
        
        // Clear the mouse events
        document.onmousemove = null;
        document.onmouseup = null;
        
        if (resumeVideo) {
            video.videoEl.play();
            video.startTrackProgress();
        }
        video.setVideoProgress(percent);
    }

}

/**
 * Sets the progress of the video element; also updates the seek bar and the displayed time
 */
VideoPlayer.prototype.setVideoProgress = function(percent) {
    this.videoEl.currentTime = percent * this.videoEl.duration;
    
    this.updateSeekBar();    
    this.updateCurrentTime();
}

/**
 * Sets the volume.
 * @param {number} volume The new volume, between 0 - 1
 */
VideoPlayer.prototype.setVolume = function(volume) {
    this.videoEl.volume = volume;
}

/**
 * Sets the fullscreen mode on or off.
 */
VideoPlayer.prototype.setFullScreen = function() {
    // Is native full screen supported?
    var fsc    = this.rootEl.requestFullScreen
                || this.rootEl.webkitRequestFullScreen
                || this.rootEl.mozRequestFullScreen,
    
        exfsc  = document.exitFullscreen 
                || document.webkitExitFullscreen 
                || document.mozCancelFullScreen;
        
    if (!this.fullscreen) {
        this.rootEl.className = this.rootEl.className + ' fullscreen';
        this.videoEl.width = document.body.offsetWidth; // backwards compatibility
        this.videoEl.removeAttribute('height');
        this.fullscreen = true;
        
        if (fsc)
            fsc.call(this.rootEl);
    } else {
        this.rootEl.className = this.rootEl.className.replace('fullscreen', '');
        this.videoEl.width = this._width; 
        this.videoEl.height = this._height;
        this.fullscreen = false;
        
        if (exfsc)
            exfsc.call(document);
    }
}

// Function that finds the X position of an element.
function findPosX(obj) {
    var curleft = obj.offsetLeft;
    while(obj = obj.offsetParent) {
        curleft += obj.offsetLeft;
    }
    return curleft;
}

// Function that returns the time in a mm:ss format
function formatTime( sec ) {
    sec = Math.round(sec);
    var min = Math.floor(sec / 60);
    sec = sec % 60;
    return (min < 10 ? '0' + min : min) + ':' + (sec < 10 ? '0' + sec : sec)
}

// IE7 support for querySelectorAll
(function(d,s){if(!document.querySelectorAll){d=document,s=d.createStyleSheet();d.querySelectorAll=function(r,c,i,j,a){a=d.all,c=[],r=r.replace(/\[for\b/gi,'[htmlFor').split(',');for(i=r.length;i--;){s.addRule(r[i],'k:v');for(j=a.length;j--;)a[j].currentStyle.k&&c.push(a[j]);s.removeRule(0)}return c}}})()

// Returns the node children of the selected element. Works cross-browser
function getChildren(el) {
    var children = [];
        
    for (var i in el.childNodes)
        if (el.childNodes[i].nodeType == 1)
            children.push( el.childNodes[i] );
    
    return children;
}

// Cross-browser script to get first child, without the text nodes
function getFirstChild(el){
    var firstChild = el.firstChild;
    while(firstChild != null && firstChild.nodeType != 1){ // skip TextNodes, comments, etc
        firstChild = firstChild.nextSibling;
    }
    return firstChild;
}

(function(){
    // Cross-browser addEventListener
    var addEventListener = (function () {
        if (window.addEventListener) {
            return function (el, ev, fn) { 
                el.addEventListener(ev, fn, false);
            };
        } else if (window.attachEvent) {
            return function (el, ev, fn) {
                el.attachEvent('on' + ev, fn);
            };
        } else {
            return function (el, ev, fn) { 
                el['on' + ev] =  fn;
            };
        }
    }());
    
    window.addEventListener = addEventListener;
})();